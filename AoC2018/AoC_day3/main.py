
f = open('input', 'r')
lines = f.readlines()
objs = {}
for line in lines:
    line = line.split(" ")
    objs[line[0]] = {}
    line[2] = line[2].replace(':','')
    objs[line[0]]['x'] = int(line[2].split(',')[0])
    objs[line[0]]['y'] = int(line[2].split(',')[1])
    objs[line[0]]['w'] = int(line[3].split('x')[0])
    objs[line[0]]['h'] = int(line[3].split('x')[1])
    objs[line[0]]['touched'] = False

#find highest x, y
hx = 0
hy = 0
for o in objs:
    o = objs[o]
    if o['x'] + o['w'] > hx:
        hx = o['x'] + o['w']
    if o['y'] + o['h'] > hy:
        hy = o['y'] + o['h']

a = [['.' for i in range(hx)] for j in range(hy)]
collision_c=0
for o in objs:
    id = o
    o = objs[o]
    #print("%d:%d"%(o['x'],o['y']))
    x,y = o['x'],o['y']
    for i in range(o['h']):
        for j in range(o['w']):
            zval = a[y+i][x+j]
            if zval == '.':
                a[y+i][x+j] = id
            elif zval == 'x':
                objs[id]['touched'] = True
            else:
                objs[id]['touched'] = True
                objs[a[y+i][x+j]]['touched'] = True
                a[y+i][x+j] = 'x'
                collision_c += 1
for i in objs:
    if objs[i]['touched'] == False:
        print(i)
print(collision_c)
