f = open('input','r')

dx = 0
tx = 0

#part 1
#for line in f:
#    lline = list(line)
#    isD = False
#    isT = False
#    for c in lline:
#        if lline.count(c) == 2:
#            isD = True
#        elif lline.count(c) == 3:
#            isT = True
#    if isD:
#        dx += 1
#    if isT:
#        tx += 1
#print(tx)
#print(dx)
#print(dx*tx)


#part 2
mindif = 10000
l1 = ""
lines = f.readlines()

for x in lines:
    for y in lines:
        os = ""
        diff = 0
        if x != y:
            for i in range(len(x)+1):
                if x[i-1]==y[i-1]:
                    os+=x[i-1]
                else:
                    os+="*"
                    diff+=1
            if diff < mindif:
                mindif = diff
                l1=os
print(mindif)
print(l1)
